var params = {
      "distance": "5000",
      "minZoom": "6",
      "maxZoom": "10",
      "nbGuess": "15",
      "centerLat": "46.23",
      "centerLng": "2.21",
      "localStorage": "{ localStorage }}",
      "tilesServer": "https://stamen-tiles-a.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png",
      "tilesAttribution": 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; <br>Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
};
