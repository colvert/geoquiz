var params = {
      "distance": "5000",
                  
      "maxZoom": "8",
                 
      "nbGuess": "10",
                 
      "localStorage": "highscore_geoquiz_nico",
                      
      "tilesServer": "https://stamen-tiles-a.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png",
                     
      "tilesAttribution": 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; <br>Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                        
};